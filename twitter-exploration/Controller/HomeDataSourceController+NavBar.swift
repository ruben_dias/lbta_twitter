//
//  HomeDataSourceController+NavBar.swift
//  twitter-exploration
//
//  Created by Ruben Dias on 11/02/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import UIKit

extension HomeDataSourceController {
    
    func setupNavigationBarItems() {
        setupRemainingNavItems()
        setupLeftNavItem()
        setupRightNavItems()
    }
    
    private func setupRemainingNavItems() {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "title_icon"))
        //titleImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        titleImageView.widthAnchor.constraint(equalToConstant: navButtonWidth).isActive = true
        titleImageView.heightAnchor.constraint(equalToConstant: navButtonHeight).isActive = true
        titleImageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = titleImageView
        
        // setup navigation bar color
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.isTranslucent = false
        
        
        // replace the default separator for the Navigation Bar (too thick)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        let navBarSeparatorView = UIView()
        navBarSeparatorView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        view.addSubview(navBarSeparatorView)
        navBarSeparatorView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
    }
    
    private func setupLeftNavItem() {
        let followButton = UIButton(type: .system)
        followButton.setImage(#imageLiteral(resourceName: "follow").withRenderingMode(.alwaysOriginal), for: .normal)
        followButton.widthAnchor.constraint(equalToConstant: navButtonWidth).isActive = true
        followButton.heightAnchor.constraint(equalToConstant: navButtonHeight).isActive = true
        followButton.contentMode = .scaleAspectFit
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: followButton)
    }
    
    private func setupRightNavItems() {
        let searchButton = UIButton(type: .system)
        searchButton.setImage(#imageLiteral(resourceName: "search").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.widthAnchor.constraint(equalToConstant: navButtonWidth).isActive = true
        searchButton.heightAnchor.constraint(equalToConstant: navButtonHeight).isActive = true
        searchButton.contentMode = .scaleAspectFit
        
        let composeButton = UIButton(type: .system)
        composeButton.setImage(#imageLiteral(resourceName: "compose").withRenderingMode(.alwaysOriginal), for: .normal)
        composeButton.widthAnchor.constraint(equalToConstant: navButtonWidth).isActive = true
        composeButton.heightAnchor.constraint(equalToConstant: navButtonHeight).isActive = true
        composeButton.contentMode = .scaleAspectFit
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: composeButton), UIBarButtonItem(customView: searchButton)]
    }
}

//
//  Service.swift
//  twitter-exploration
//
//  Created by Ruben Dias on 08/03/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import Foundation
import TRON
import SwiftyJSON

struct Service {
    
    static let sharedInstance = Service()
    let tron = TRON(baseURL: "https://api.letsbuildthatapp.com")
    
    func fetchHomeFeed(completion: @escaping (HomeDataSource?, Error?) -> ()) {
        let request: APIRequest<HomeDataSource, JSONError> = tron.swiftyJSON.request("/twitter/home")
        
        request.perform(withSuccess: { (homeDataSource) in
            print("Successfully fetched JSON objects.")
            completion(homeDataSource, nil)
        }) { (error) in
            completion(nil, error)
        }
    }
    
    class JSONError: JSONDecodable {
        required init(json: JSON) throws {
            print("JSON Error.")
        }
    }
}

//
//  Tweet.swift
//  twitter-exploration
//
//  Created by Ruben Dias on 19/02/2018.
//  Copyright © 2018 Ruben Dias. All rights reserved.
//

import LBTAComponents
import SwiftyJSON
import TRON

struct Tweet: JSONDecodable {
    let user: User
    let message: String
    
    init(json: JSON) {
        let userJSON = json["user"]
        self.user = User(json: userJSON)
        self.message = json["message"].stringValue
    }
}
